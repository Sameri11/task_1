## **Simple url checker**

#### For check urls:
<code>$ bash url_checker.sh
</code>

#### Output:
**Exampes:**

<code>Success: response code for \<URL> - 302</code>

<code>Failure: response code for \<URL> - 404</code>

Script stops as soon as invalid response (failure) code returned
