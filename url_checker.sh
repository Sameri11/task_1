#!/bin/bash
set -e
function url_checker {
FILE="./urls.txt"
if [ -f "$FILE" ]; then
  while read URL
    do
      response=$(curl -o /dev/null -I -s -w "%{http_code}\n" $URL)
      case "$response" in
        [2-3][0-9][0-9]) echo "Success: response code for $URL - $response" ;;
        [4-5][0-9][0-9]) echo "Failure: response code for $URL - $response"
                     return 1 ;;
      esac
    done < "$FILE"
else
  echo "URLs file not found"
fi
}
url_checker

